class Api::V1::CompaniesController < Api::V1::BaseController

  # Não estava conseguindo autenticar, sempre que eu tentava retornava erro, mas com essa linha abaixo sempre chama o metodo de autenticação
  #quando vai chamar alguma função do controller
  #before_action :authenticate_user!

  #Every time the show request was called the set company method will be called first returning the chosed id.
  before_action :set_company, only: [:show]

  def index
      if (params.has_key? :name and params.has_key? :type) and (params[:name] != "" and params[:type] != "")
        @companies = Company.where('name LIKE ? and company_type = ?', "%#{params[:name]}%", params[:type])
      elsif params.has_key? :name and params[:name] != ""
        @companies = Company.where('name LIKE ?', "%#{params[:name]}%")
      elsif params.has_key? :type and params[:type] != ""
        @companies = Company.where(company_type: params[:type])
      else
        @companies = Company.all
      end
    end

    def show

    end

  private def set_company
    @company = Company.find(params[:id])
  end

end
