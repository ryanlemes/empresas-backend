json.id           @company.id
json.name         @company.name
json.cnpj         @company.cnpj
json.company_type @company.company_type
json.description  @company.description
json.address      @company.address
