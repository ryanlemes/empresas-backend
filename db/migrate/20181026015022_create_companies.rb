class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      #Collumns definitions
      t.string   :name,          null: false, default: ""
      t.string   :company_type,  null: false, default: ""
      t.string   :description,   null: false, default: ""
      t.string   :address,       null: true,  default: ""
      t.string   :cnpj,          null: true,  default: ""

      t.timestamps
    end
  end
end
